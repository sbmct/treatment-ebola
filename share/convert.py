from census_maps.csv2dbf import merge_csv_into_dbf
from census_maps.shp_dbf_to_svg import render_shapefile, get_dbf_records_iter
from census_maps.jenks import get_jenks_colors
import os.path
import pprint

#SHP_DIR = os.path.join(os.path.dirname(__file__), '..', 'tl_2010_us_county10')
#shp_path = os.path.join(SHP_DIR, 'places.dbf')
#dbf_path = os.path.join(SHP_DIR, 'places.dbf')
#shp_path = os.path.join(SHP_DIR, 'places_filt.shp')
#dbf_path = os.path.join(SHP_DIR, 'placese_filt.dbf')

for i in os.listdir('my_csvs'):
    directory = os.getcwd()
    fullpath = os.path.join(directory,'my_csvs',i)
    merged_dbf = i[:-3]+'.dbf'
    merge_csv_into_dbf('SLE_Adm2_1m_gov.dbf',
                       fullpath, merged_dbf,
                       dbf_id_col='DISTRICT', csv_id_col='DISTRICT')
    
    
    field_names, iter_records = get_dbf_records_iter(merged_dbf)
    ##Find the index of the income field
    ##hc02_est_v is HC02_EST_VC02,Mean income (dollars); Estimate; All households
    f_ind = field_names.index('New Confir')
    num_groups  = 8
    color_group = 'Blues'
    generated_colors = []
    for j in list(iter_records):
        if j[5]==0:
            colors= [255/256.,255/256.,212/256.]
        elif j[5]<5:
            colors= [254/256.,217/256.,142/256.]
        elif j[5]<15:
            colors= [254/256.,153/256.,41/256.]
        elif j[5]<25:
            colors= [217/256.,95/256.,14/256.]
        else:
            colors= [153/256.,52/256.,4/256.]
        generated_colors.append(colors)

    ##Make an iterator for jenks colors
    print generated_colors
    #colors = get_jenks_colors(iter_records, f_ind, num_groups, color_group,outlier_flatten=0.1)
    ##Render the shapefile using the jenks colors
    render_shapefile('SLE_Adm2_1m_gov.shp', 'SLE-outbreak'+str(i[3:-3])+'.svg', colors=generated_colors)
