import math
from cStringIO import StringIO
import numpy as np
import matplotlib
matplotlib.use('Agg') #non-interactive backend for writing to files
import matplotlib.pyplot as plt
import scipy.ndimage as ndi
from sluice.geometry import Location

def generate(name, attributes, px, time, live, passthrough, data, bounds):
    w = int(bounds.w)
    h = int(bounds.h)
    grid = np.zeros((h+1, w+1))

    for ent in data:
        x, y = bounds.project(Location(ent['loc'][0], ent['loc'][1]))
        val = ent.get('val')
        try:
            val = float(val)
        except (ValueError, TypeError):
            #catches if the value is an empty string, None, or text
            val = 1
        grid[int(y)][int(x)] = val

    #Guassian filter to get the heatmap effect
    dotwidth = 32
    r = math.sqrt(dotwidth)
    grid = ndi.gaussian_filter(grid, (r, r))

    #Draw the image
    new_fig = plt.figure(None, figsize=(w/100.0, h/100.0), dpi=100.0)
    cmap = plt.get_cmap('jet')
    cmap.set_under([0.0, 0.0, 0.0, 0.0])
    plt.imshow(grid, cmap=cmap, origin='lower', vmin=1e-6)

    #turn off axes, ticks, etc.
    new_fig.tight_layout(pad=0)
    a = new_fig.gca()
    a.set_frame_on(False)
    a.set_xticks([])
    a.set_yticks([])
    plt.axis('off')
    plt.xlim(0, w)
    plt.ylim(h, 0)

    #Generate PNG data for the plot
    png = StringIO()
    plt.savefig(png, format='png', bbox_inches='tight', transparent=True,
                pad_inches=0)
    png.seek(0)
    return png, w, h


if __name__ == '__main__':
    from conduce.texture.compat_daemon import DecoratedTextureDaemonCompat
    t = DecoratedTextureDaemonCompat('HeatmapDaemon', texture_generator=generate)
    t.run()
