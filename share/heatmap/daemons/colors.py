from PIL import Image

from sluice.api import Sluice

colormaps = {
    'red': {
        'red': (
            (0.0, 1.0, 1.0),
            (1.0, 0.8, 0.8),
        ),
        'green': (
            (0.0, 1.0, 1.0),
            (0.7, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
        'blue': (
            (0.0, 1.0, 1.0),
            (0.7, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
        'alpha': (
            (0.0, 0.0, 0.0),
            (0.5, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        )
    },
    'green': {
        'red': (
            (0.0, 1.0, 1.0),
            (0.7, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
        'green': (
            (0.0, 1.0, 1.0),
            (1.0, 0.9, 0.9),
        ),
        'blue': (
            (0.0, 1.0, 1.0),
            (0.7, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
        'alpha': (
            (0.0, 0.0, 0.0),
            (0.5, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        )
    },
    'blue': {
        'red': (
            (0.0, 1.0, 1.0),
            (0.7, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
        'green': (
            (0.0, 1.0, 1.0),
            (0.7, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
        'blue': (
            (0.0, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        ),
        'alpha': (
            (0.0, 0.0, 0.0),
            (0.5, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        )
    },
}

def get_colormap(name):
    if name not in colormaps:
        fn = Sluice.get_share_file(name)
        img = Image.open(fn)
        cmap = { 'red': [], 'green': [], 'blue': [], 'alpha': [] }
        for i in xrange(img.size[1]):
            stop = 1.0 * i / (img.size[1] - 1)
            px = img.getpixel((1, i))
            cmap['red'].append((stop, px[0]/255.0, px[0]/255.0))
            cmap['green'].append((stop, px[1]/255.0, px[1]/255.0))
            cmap['blue'].append((stop, px[2]/255.0, px[2]/255.0))
            cmap['alpha'].append((stop, px[3]/255.0, px[3]/255.0))
        colormaps[name] = cmap
    return colormaps[name]

