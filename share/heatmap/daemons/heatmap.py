from conduce.texture.compat_daemon import DecoratedTextureDaemonCompat
from conduce.texture.utils import png
#Pillow is a drop in replacement for PIL, so you still import from PIL
from PIL import Image, ImageDraw
from conduce.texture.daemon import Location

def generate(name, attributes, px, time, live, passthrough, data, bounds):
    color = attributes['custom_attr_name'][0]
    print "generating with color", color
    w = int(bounds.w)
    h = int(bounds.h)
    img = Image.new("RGBA", (w, h))
    draw = ImageDraw.Draw(img)
    for item in data:
        #Convert lat/long to x/y
        item_loc = Location(item['loc'][0], item['loc'][1])
        center_x, center_y = bounds.project(item_loc)
        #Calculate circle radius: Tier 1 = 15px, Tier 2 = 10 px, Tier 3 = 5 px
        dim = 20 - (int(item['val'])*5)
        #draw a circle
        draw.ellipse((center_x-dim, center_y-dim, center_x+dim, center_y+dim),
                     fill=color)
    return png(img), w, h

if __name__ == '__main__':
    t = DecoratedTextureDaemonCompat('HeatmapDaemon',
                                     texture_generator=generate)
    t.run()
